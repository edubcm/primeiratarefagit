git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init
Usa-se para inicializar um novo repositório vazio na pasta selecionada

git status
Usa-se para verificar em qual diretório e branch estamos trabalhando. Também indica quais arquivos estão prontos ou não para o receber o commit

git add arquivo/diretório
Usa-se para salvar alterações no arquivo do estado atual do trabalho para posteriormente dar commit, ou seja, o git add não salva permanentemente na linha do tempo

git add --all = git add .
Usa-se para salvar alterações de todos os arquivos do estado atual para posteriormente dar commit

git commit -m “Primeiro commit”
Usa-se para dar commit com a mensagem "Primeiro commit"
-------------------------------------------------------

git log
Usa-se para verificar todo o histórico de alterações feitas em um repositório

git log arquivo
Usa-se para verificar todo o histórico de alterações feitas em um arquivo do repositório

git reflog
Usa-se para verificar o histórico de commits dados em um branch

-------------------------------------------------------

git show
Usa-se para verificar as alterações feitas pelo autor do último commit feito em um branch

git show <commit>
Usa-se para verificar as alterações feitas em algum commit específico pelo autor em um branch

-------------------------------------------------------

git diff
Usa-se para rastrear a diferença entre mudanças feitas em um arquivo

git diff <commit1> <commit2>
Usa-se para rastrear a diferença entre dois arquivos

-------------------------------------------------------

git reset --hard <commit>
Usa-se para descartar todas as alterações em fase de stage e reverte todos os commits feitos até determinado ponto (que é o commit especificado no comando)

-------------------------------------------------------

git branch
Usa-se para listar todos os branch em dado repositório e verificar qual branch está sendo trabalhado no momento

git branch -r 
Usa-se para listara branch remotos

git branch -a
Usa-se para listar todos os branch remotos e locais

git branch -d <branch_name> 
Usa-se para excluir o branch especificado, mas pode ser segurada pelo Git caso tenha mudanças que não sofreram merge

git branch -D <branch_name> 
Usa-se para forçar exclusão do branch especificado, independente que tenha ou não mudanças que sofreram merge

git branch -m <nome_novo>
Usa-se para renomear o branch atual para o novo nome especificado

git branch -m <nome_antigo> <nome_novo>
Usa-se para renomear um branch específico através do nome_antigo para o nome_novo

-------------------------------------------------------

git checkout <branch_name> 
Usa-se para selecionar e trabalhar o branch especificado 

git checkout -b <branch_name> 
Usa-se para criar, selecionar e trabalhar um branch

-------------------------------------------------------

git merge <branch_name>
Usa-se para mesclar um branch especificado ao branch atual que está sendo trabalhado

-------------------------------------------------------

git clone
Usa-se para clonar um repositório

git pull
Usa-se para atualizar o repositório local com o conteúdo inserido a um repositório remoto

git push
Usa-se para atualizar o repositório remoto com o conteúdo inserido a um repositório local

-------------------------------------------------------

git remote -v
Usa-se para listar as conexões remotas e seus URLs feitos com outros repositórios

git remote add origin <url>
Usa-se para estabelecer uma nova conexão com um repositório remoto

git remote <url> origin
Usa-se para alterar o URL do repositório original

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


